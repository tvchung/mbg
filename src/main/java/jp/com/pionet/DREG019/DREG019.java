package jp.com.pionet.DREG019;

import java.io.Serializable;

public class DREG019 extends DREG019Key implements Serializable {
    private String CONSULTATIONINFO;

    private Short INFOTYPE;

    private String REMARKSMEMO;

    private static final long serialVersionUID = 1L;

    public String getCONSULTATIONINFO() {
        return CONSULTATIONINFO;
    }

    public void setCONSULTATIONINFO(String CONSULTATIONINFO) {
        this.CONSULTATIONINFO = CONSULTATIONINFO == null ? null : CONSULTATIONINFO.trim();
    }

    public Short getINFOTYPE() {
        return INFOTYPE;
    }

    public void setINFOTYPE(Short INFOTYPE) {
        this.INFOTYPE = INFOTYPE;
    }

    public String getREMARKSMEMO() {
        return REMARKSMEMO;
    }

    public void setREMARKSMEMO(String REMARKSMEMO) {
        this.REMARKSMEMO = REMARKSMEMO == null ? null : REMARKSMEMO.trim();
    }
}