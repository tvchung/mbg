package jp.com.pionet.DREG019;

import java.io.Serializable;

public class DREG019Key implements Serializable {
    private Short INFONUMBER;

    private Short ID;

    private static final long serialVersionUID = 1L;

    public Short getINFONUMBER() {
        return INFONUMBER;
    }

    public void setINFONUMBER(Short INFONUMBER) {
        this.INFONUMBER = INFONUMBER;
    }

    public Short getID() {
        return ID;
    }

    public void setID(Short ID) {
        this.ID = ID;
    }
}