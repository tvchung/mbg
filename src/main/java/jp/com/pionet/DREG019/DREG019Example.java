package jp.com.pionet.DREG019;

import java.util.ArrayList;
import java.util.List;

public class DREG019Example {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DREG019Example() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andINFONUMBERIsNull() {
            addCriterion("INFONUMBER is null");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERIsNotNull() {
            addCriterion("INFONUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andINFONUMBEREqualTo(Short value) {
            addCriterion("INFONUMBER =", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERNotEqualTo(Short value) {
            addCriterion("INFONUMBER <>", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERGreaterThan(Short value) {
            addCriterion("INFONUMBER >", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERGreaterThanOrEqualTo(Short value) {
            addCriterion("INFONUMBER >=", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERLessThan(Short value) {
            addCriterion("INFONUMBER <", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERLessThanOrEqualTo(Short value) {
            addCriterion("INFONUMBER <=", value, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERIn(List<Short> values) {
            addCriterion("INFONUMBER in", values, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERNotIn(List<Short> values) {
            addCriterion("INFONUMBER not in", values, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERBetween(Short value1, Short value2) {
            addCriterion("INFONUMBER between", value1, value2, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andINFONUMBERNotBetween(Short value1, Short value2) {
            addCriterion("INFONUMBER not between", value1, value2, "INFONUMBER");
            return (Criteria) this;
        }

        public Criteria andIDIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIDIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIDEqualTo(Short value) {
            addCriterion("ID =", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDNotEqualTo(Short value) {
            addCriterion("ID <>", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDGreaterThan(Short value) {
            addCriterion("ID >", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDGreaterThanOrEqualTo(Short value) {
            addCriterion("ID >=", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDLessThan(Short value) {
            addCriterion("ID <", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDLessThanOrEqualTo(Short value) {
            addCriterion("ID <=", value, "ID");
            return (Criteria) this;
        }

        public Criteria andIDIn(List<Short> values) {
            addCriterion("ID in", values, "ID");
            return (Criteria) this;
        }

        public Criteria andIDNotIn(List<Short> values) {
            addCriterion("ID not in", values, "ID");
            return (Criteria) this;
        }

        public Criteria andIDBetween(Short value1, Short value2) {
            addCriterion("ID between", value1, value2, "ID");
            return (Criteria) this;
        }

        public Criteria andIDNotBetween(Short value1, Short value2) {
            addCriterion("ID not between", value1, value2, "ID");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOIsNull() {
            addCriterion("CONSULTATIONINFO is null");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOIsNotNull() {
            addCriterion("CONSULTATIONINFO is not null");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOEqualTo(String value) {
            addCriterion("CONSULTATIONINFO =", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFONotEqualTo(String value) {
            addCriterion("CONSULTATIONINFO <>", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOGreaterThan(String value) {
            addCriterion("CONSULTATIONINFO >", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOGreaterThanOrEqualTo(String value) {
            addCriterion("CONSULTATIONINFO >=", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOLessThan(String value) {
            addCriterion("CONSULTATIONINFO <", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOLessThanOrEqualTo(String value) {
            addCriterion("CONSULTATIONINFO <=", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOLike(String value) {
            addCriterion("CONSULTATIONINFO like", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFONotLike(String value) {
            addCriterion("CONSULTATIONINFO not like", value, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOIn(List<String> values) {
            addCriterion("CONSULTATIONINFO in", values, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFONotIn(List<String> values) {
            addCriterion("CONSULTATIONINFO not in", values, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFOBetween(String value1, String value2) {
            addCriterion("CONSULTATIONINFO between", value1, value2, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andCONSULTATIONINFONotBetween(String value1, String value2) {
            addCriterion("CONSULTATIONINFO not between", value1, value2, "CONSULTATIONINFO");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEIsNull() {
            addCriterion("INFOTYPE is null");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEIsNotNull() {
            addCriterion("INFOTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEEqualTo(Short value) {
            addCriterion("INFOTYPE =", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPENotEqualTo(Short value) {
            addCriterion("INFOTYPE <>", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEGreaterThan(Short value) {
            addCriterion("INFOTYPE >", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEGreaterThanOrEqualTo(Short value) {
            addCriterion("INFOTYPE >=", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPELessThan(Short value) {
            addCriterion("INFOTYPE <", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPELessThanOrEqualTo(Short value) {
            addCriterion("INFOTYPE <=", value, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEIn(List<Short> values) {
            addCriterion("INFOTYPE in", values, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPENotIn(List<Short> values) {
            addCriterion("INFOTYPE not in", values, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPEBetween(Short value1, Short value2) {
            addCriterion("INFOTYPE between", value1, value2, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andINFOTYPENotBetween(Short value1, Short value2) {
            addCriterion("INFOTYPE not between", value1, value2, "INFOTYPE");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOIsNull() {
            addCriterion("REMARKSMEMO is null");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOIsNotNull() {
            addCriterion("REMARKSMEMO is not null");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOEqualTo(String value) {
            addCriterion("REMARKSMEMO =", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMONotEqualTo(String value) {
            addCriterion("REMARKSMEMO <>", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOGreaterThan(String value) {
            addCriterion("REMARKSMEMO >", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOGreaterThanOrEqualTo(String value) {
            addCriterion("REMARKSMEMO >=", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOLessThan(String value) {
            addCriterion("REMARKSMEMO <", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOLessThanOrEqualTo(String value) {
            addCriterion("REMARKSMEMO <=", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOLike(String value) {
            addCriterion("REMARKSMEMO like", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMONotLike(String value) {
            addCriterion("REMARKSMEMO not like", value, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOIn(List<String> values) {
            addCriterion("REMARKSMEMO in", values, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMONotIn(List<String> values) {
            addCriterion("REMARKSMEMO not in", values, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMOBetween(String value1, String value2) {
            addCriterion("REMARKSMEMO between", value1, value2, "REMARKSMEMO");
            return (Criteria) this;
        }

        public Criteria andREMARKSMEMONotBetween(String value1, String value2) {
            addCriterion("REMARKSMEMO not between", value1, value2, "REMARKSMEMO");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}