package jp.com.pionet.DREG019;

import java.util.List;
import jp.com.pionet.DREG019.DREG019;
import jp.com.pionet.DREG019.DREG019Example;
import jp.com.pionet.DREG019.DREG019Key;
import org.apache.ibatis.annotations.Param;

public interface DREG019Mapper {
    long countByExample(DREG019Example example);

    int deleteByExample(DREG019Example example);

    int deleteByPrimaryKey(DREG019Key key);

    int insert(DREG019 record);

    int insertSelective(DREG019 record);

    List<DREG019> selectByExample(DREG019Example example);

    DREG019 selectByPrimaryKey(DREG019Key key);

    int updateByExampleSelective(@Param("record") DREG019 record, @Param("example") DREG019Example example);

    int updateByExample(@Param("record") DREG019 record, @Param("example") DREG019Example example);

    int updateByPrimaryKeySelective(DREG019 record);

    int updateByPrimaryKey(DREG019 record);
}